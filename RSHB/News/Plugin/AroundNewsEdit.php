<?php

namespace RSHB\News\Plugin;

class AroundNewsEdit
{
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        array $data = []
    ) {
        $this->request = $request;
    }


    public function aroundsave(\RSHB\News\Model\NewsRepository $subject, callable $proceed, $news)
    {
        //echo __METHOD__ . ' - Before proceed() </br>';
        $news->setIntrotext('Before proceed');
        $result = $proceed($news);
        //  echo __METHOD__ . ' - After proceed() </br>';
        $news->setImage('After proceed');
        $news->save();
        return $result;
    }

}

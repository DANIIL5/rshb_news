<?php
/**
 * Created by PhpStorm.
 * User: DANIIL
 * Date: 30.06.2020
 * Time: 13:24
 */

namespace RSHB\News\Plugin;

class BeforeNewsEdit
{
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        array $data = []
    ) {
        $this->request = $request;
    }


    public function beforesave(\RSHB\News\Model\NewsRepository $subject, $news)
    {
        $news->setTitle('before_plugin_working');
    }

}

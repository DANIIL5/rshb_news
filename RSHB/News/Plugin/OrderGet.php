<?php

namespace RSHB\News\Plugin;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\Data\OrderExtensionFactory;
use Magento\Sales\Api\Data\OrderExtensionInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderSearchResultInterface;

/**
 * Class OrderRepositoryPlugin
 */
class OrderGet
{
    protected $orderExtensionFactory;

    protected $attributeFactory;

    public function __construct(
        \Magento\Sales\Api\Data\OrderExtensionFactory $orderExtensionFactory,
        \RSHB\News\Model\AttributeFactory $attributeFactory
    ) {
        $this->orderExtensionFactory = $orderExtensionFactory;
        $this->attributeFactory = $attributeFactory;
    }

    public function afterGet(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        \Magento\Sales\Api\Data\OrderInterface $resultOrder
    ) {
        $resultOrder = $this->getCustomAttribute($resultOrder);
        return $resultOrder;
    }

    private function getCustomAttribute(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        try {
            $customAttribute = $this->attributeFactory->create();
            $single_customAttribute = $customAttribute->getResourceCollection()->addFieldToFilter(
                'order_id',
                $order->getEntityId()
            )->getFirstItem();
            $customAttribute->load($order->getEntityId(), 'order_id');
            if (!$single_customAttribute->getEntityId()) {
                throw new NoSuchEntityException();
            }
        } catch (NoSuchEntityException $e) {
            return $order;
        }

        $extensionAttributes = $order->getExtensionAttributes();
        $orderExtension = $extensionAttributes ? $extensionAttributes : $this->orderExtensionFactory->create();

        $orderExtension->setOrderCustomAttribute($single_customAttribute);
        $order->setExtensionAttributes($orderExtension);

        return $order;
    }
}

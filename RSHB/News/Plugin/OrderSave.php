<?php

namespace RSHB\News\Plugin;
use \Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Exception\CouldNotSaveException;

class OrderSave
{
    private $AttributeInterfaceFactory;
    protected $attributeFactory;

    public function __construct(
        \Magento\Sales\Api\Data\OrderExtensionFactory $orderExtensionFactory,
        \RSHB\News\Model\AttributeFactory $attributeFactory
    ) {
        $this->orderExtensionFactory = $orderExtensionFactory;
        $this->attributeFactory = $attributeFactory;
    }

    public function afterSave(
        OrderRepositoryInterface $subject,
        \Magento\Sales\Api\Data\OrderInterface $resultOrder
    ) {
        $resultOrder = $this->saveCustomAttribute($resultOrder,$subject);
        return $resultOrder;
    }

    private function saveCustomAttribute($order,$subject)
    {
     try {
        $order = $subject->get($order->getEntityId());
        $extensionAttribute = $order->getExtensionAttributes();
        if ($extensionAttribute->getOrderCustomAttribute()) {
            $orderCustomAttribute = $extensionAttribute->getOrderCustomAttribute();
        } else {
            $orderCustomAttribute =  $this->attributeFactory->create();
        }
        $extensionAttribute->setOrderCustomAttribute($orderCustomAttribute);
        if ($orderCustomAttribute) {
            $orderCustomAttribute->setBar('898989');
            $orderCustomAttribute->setOrderId($order->getEntityId());
        }
        $extensionAttributes = $order->getExtensionAttributes();
        if (null !== $extensionAttributes && null !== $extensionAttributes->getOrderCustomAttribute()) {
            /* @var \Magento\GiftMessage\Api\Data\MessageInterface $giftMessage */
            $customAttribute = $extensionAttributes->getOrderCustomAttribute();
            try {
                // The actual implementation of the repository is omitted
                // but it is where you would load your value from the database (or any other persistent storage)
                // $customAttribute = $this->attributeRepository->save($order->getEntityId());

                // We use model save for example
                $customAttribute->save();
            } catch (\Exception $e) {
                throw new CouldNotSaveException();
            }
        }
        } catch (\Exception $e) {
            throw new CouldNotSaveException();
        }
        return $order;
    }
}

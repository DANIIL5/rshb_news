<?php

namespace RSHB\News\Cron;

use Psr\Log\LoggerInterface;
use RSHB\News\Model\NewsRepository;

class CreateNews extends \Magento\Framework\App\Action\Action
{
    protected $logger;
    protected $_newsRepository;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \RSHB\News\Model\NewsFactory $newsFactory,
        NewsRepository $newsRepository,
        LoggerInterface $logger
    ) {
        $this->_newsFactory = $newsFactory;
        $this->_newsRepository = $newsRepository;
        $this->logger = $logger;
        return parent::__construct($context);
    }

    public function execute()
    {
        $news = $this->_newsFactory->create();
        $news->setTitle('Test title');
        $news->setIntrotext('Test introtext');
        $this->_newsRepository->save($news);
        $this->logger->info('Cron Works');
    }
}

<?php

namespace RSHB\News\Model\ResourceModel\Attribute;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('RSHB\News\Model\Attribute', 'RSHB\News\Model\ResourceModel\Attribute');
    }
}

<?php

namespace RSHB\News\Model\ResourceModel\News;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'Id';
    protected $_eventPrefix = 'rshb_news';
    protected $_eventObject = 'news_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\RSHB\News\Model\News::class, \RSHB\News\Model\ResourceModel\News::class);
    }

}

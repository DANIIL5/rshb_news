<?php

namespace RSHB\News\Model\ResourceModel;

class Attribute extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('order_custom_attribute', 'entity_id');
    }
}

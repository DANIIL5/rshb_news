<?php

namespace RSHB\News\Model;

use Magento\Customer\Model\CustomerRegistry;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use RSHB\News\Api\Data\NewsInterface;
use RSHB\News\Api\Data\NewsSearchResultInterface;
use RSHB\News\Api\NewsRestApiInterface;
use RSHB\News\Model\ResourceModel\News as ResourceNews;
use RSHB\News\Model\ResourceModel\News\Collection as NewsCollectionFactory;
use RSHB\News\Api\Data\NewsSearchResultInterfaceFactory;

class NewsRestApi implements NewsRestApiInterface
{
    /**
     * @var CustomerRegistry
     */
    /**
     * @var ResourceNews
     */
    protected $resource;
    /**
     * @var NewsFactory
     */
    protected $newsFactory;
    protected $customerRegistry;
    /**
     * @var NewsInterface[]
     */
    protected $instances = [];
    /**
     * @var NewsSearchResultInterfaceFactory
     */
    private $newsSearchResultFactory;
    /**
     * @var NewsCollectionFactory
     */
    private $newsCollectionFactory;

    /**
     * @param ResourceNews $resource
     * @param \RSHB\News\Model\NewsFactory $newsFactory
     * @param NewsCollectionFactory $newsCollectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param NewsSearchResultInterfaceFactory newsSearchResultFactory
     */
    public function __construct(
        ResourceNews $resource,
        NewsFactory $newsFactory,
        NewsCollectionFactory $newsCollectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        CustomerRegistry $customerRegistry,
        NewsSearchResultInterfaceFactory $newsSearchResultFactory
    ) {
        $this->resource = $resource;
        $this->newsFactory = $newsFactory;
        $this->newsCollectionFactory = $newsCollectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->customerRegistry = $customerRegistry;
        $this->newsSearchResultFactory = $newsSearchResultFactory;
    }

    /**
     * Get customer's name by Customer ID and return greeting message.
     *
     * @param int $customerId
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException If customer with the specified ID does not exist.
     * @throws \Magento\Framework\Exception\LocalizedException
     * @api
     */

    public function sayHello(int $customerId)
    {
        $customerModel = $this->customerRegistry->retrieve($customerId);
        $name = $customerModel->getDataModel()->getFirstname();
        return "Hello " . $name;
    }

    /**
     * @param int $newsId
     * @return NewsInterface
     * @throws NoSuchEntityException
     * *@api
     */
    public function getById(int $newsId)
    {
        if (!isset($this->instances[$newsId])) {
            $news = $this->newsFactory->create();
            $this->resource->load($news, $newsId);
            if (!$news->getId()) {
                throw new NoSuchEntityException(__('News with id "%1" does not exist.', $newsId));
            }
            $this->instances[$newsId] = $news;
        }

        return $this->instances[$newsId];
    }

    /**
     * @param NewsInterface $news
     * @return bool true on success
     * @throws CouldNotDeleteException
     */
    public function delete(newsInterface $news)
    {
        try {
            $faqId = $news->getId();
            $this->resource->delete($news);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        unset($this->instances[$faqId]);
        return true;
    }

    /**
     * @param int $newsId
     * @return bool true on success
     */
    public function deleteById($newsId)
    {
        return $this->delete($this->getById($newsId));
    }


    /**
     * @param NewsInterface $news
     * @return NewsInterface
     * @throws CouldNotSaveException
     */
    public function save(NewsInterface $news)
    {
        try {
            $this->resource->save($news);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        unset($this->instances[$news->getId()]);
        return $news;
    }


    /**
     * @param NewsInterface $news
     * @return NewsInterface
     * @throws NoSuchEntityException
     * *@api
     */
    public function editStatus(NewsInterface $news)
    {
        try {
            $mynews = $this->getById($news->getId());
            $status = $news->getStatus();
            $mynews->setStatus((int)$status);
            $this->save($mynews);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $news;
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \RSHB\News\Api\Data\NewsSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var NewsCollectionFactory $collection */
        $collections = $this->newsFactory->create();
        $collection = $collections->getCollection();
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        /** @var NewsSearchResultInterface $searchResult */
        $collections = $this->newsFactory->create();
        $collection = $collections->getCollection();
        $searchResult = $this->newsSearchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());
        return $searchResult;
    }
}

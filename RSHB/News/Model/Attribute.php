<?php

namespace RSHB\News\Model;

class Attribute extends \Magento\Framework\Model\AbstractModel implements \RSHB\News\Api\Data\AttributeInterface
{
    protected function _construct()
    {
        $this->_init('RSHB\News\Model\ResourceModel\Attribute');
    }

    public function setOrderId($value)
    {
        return $this->setData('order_id', $value);
    }

    public function getOrderId()
    {
        return $this->getData('order_id');
    }

    public function setBar($value)
    {
        return $this->setData('bar', $value);
    }

    public function getBar()
    {
        return $this->getData('bar');
    }

}

<?php

namespace RSHB\News\Model;

class Bowl
{
    public $fruits;

    public function __construct(array $fruits = [])
    {
        $this->fruits = $fruits;
    }
}

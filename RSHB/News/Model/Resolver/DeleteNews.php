<?php


declare(strict_types=1);

namespace RSHB\News\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use RSHB\News\Model\NewsRepository;

/**
 * Order sales field resolver, used for GraphQL request processing
 */
class DeleteNews implements ResolverInterface
{
    /**
     * @var NewsRepository
     */
    private $newsRepository;

    public function __construct(
        NewsRepository $newsRepository
    ) {
        $this->newsRepository = $newsRepository;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $news = $this->newsRepository->deleteById($args['news_id']);
        if ($news) {
            return true;
        } else {
            return false;
        }
    }
}

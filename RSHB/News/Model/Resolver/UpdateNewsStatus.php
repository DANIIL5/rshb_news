<?php


declare(strict_types=1);

namespace RSHB\News\Model\Resolver;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use RSHB\News\Api\Data\NewsInterface;
use RSHB\News\Model\NewsRepository;

/**
 * Order sales field resolver, used for GraphQL request processing
 */
class UpdateNewsStatus implements ResolverInterface
{
    /**
     * @var NewsRepository
     */
    private $newsRepository;

    public function __construct(
        NewsRepository $newsRepository
    ) {
        $this->newsRepository = $newsRepository;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $mynews = $this->newsRepository->getById($args['news_id']);
        $mynews->setStatus((int)$args['Status']);
        $mynews = $this->newsRepository->save($mynews);
        return $mynews;
    }
}

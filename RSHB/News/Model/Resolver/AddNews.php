<?php


declare(strict_types=1);

namespace RSHB\News\Model\Resolver;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use RSHB\News\Api\Data\NewsInterface;
use RSHB\News\Model\NewsRepository;
use RSHB\News\Model\ResourceModel\News as ResourceNews;

/**
 * Order sales field resolver, used for GraphQL request processing
 */
class AddNews implements ResolverInterface
{
    /**
     * @var NewsRepository
     */
    private $newsRepository;
    /**
     * @var \RSHB\News\Model\NewsFactory
     */
    private $_newsFactory;
    private $_newsRepository;

    public function __construct(
        NewsRepository $newsRepository,
        ResourceNews $resource,
        \RSHB\News\Model\NewsFactory $newsFactory
    ) {
        $this->newsRepository = $newsRepository;
        $this->resource = $resource;
        $this->_newsFactory = $newsFactory;
    }

    /**
     * @inheritdoc{
     * "news":{
     * "Title":"Wreewrwr",
     * "Introtext":"sdfsdfds"
     * }
     * }
     *
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $news = $this->_newsFactory->create();
        $news->setTitle($args['Title']);
        $news->setIntrotext($args['Introtext']);
        $new = $this->newsRepository->save($news);
        return $new;
    }
}

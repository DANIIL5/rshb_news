<?php

// app/code/[Vendor]/[Module]/Model/FaqRepository.php


namespace RSHB\News\Model;

use RSHB\News\Api\Data\NewsInterface;
use RSHB\News\Api\NewsRepositoryInterface;
use RSHB\News\Model\ResourceModel\News as ResourceNews;
use RSHB\News\Model\ResourceModel\News\Collection as NewsCollectionFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use RSHB\News\Api\Data\NewsSearchResultInterface;
use RSHB\News\Api\Data\NewsSearchResultInterfaceFactory;

class NewsRepository implements NewsRepositoryInterface
{

    /**
     * @var ResourceNews
     */
    protected $resource;

    /**
     * @var NewsFactory
     */
    protected $newsFactory;

    /**
     * @var NewsCollectionFactory
     */
    protected $newsCollectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var NewsInterface[]
     */
    protected $instances = [];

    /**
     * @var NewsSearchResultInterfaceFactory
     */
    private $newsSearchResultFactory;


    /**
     * @param ResourceNews $resource
     * @param \RSHB\News\Model\NewsFactory $newsFactory
     * @param NewsCollectionFactory $newsCollectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param NewsSearchResultInterfaceFactory newsSearchResultFactory
     */
    public function __construct(
        ResourceNews $resource,
        NewsFactory $newsFactory,
        NewsCollectionFactory $newsCollectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        NewsSearchResultInterfaceFactory $newsSearchResultFactory
    ) {
        $this->resource = $resource;
        $this->newsFactory = $newsFactory;
        $this->newsCollectionFactory = $newsCollectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->newsSearchResultFactory = $newsSearchResultFactory;
    }

    /**
     * @param NewsInterface $news
     * @return NewsInterface
     * @throws CouldNotSaveException
     */
    public function save(NewsInterface $news)
    {
        try {
            $this->resource->save($news);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        unset($this->instances[$news->getId()]);
        return $news;
    }


    /**
     * @param int $newsId
     * @return NewsInterface
     * @throws NoSuchEntityException
     */
    public function getById($newsId)
    {
        if (!isset($this->instances[$newsId])) {
            $news = $this->newsFactory->create();
            $this->resource->load($news, $newsId);
            if (!$news->getId()) {
                throw new NoSuchEntityException(__('News with id "%1" does not exist.', $newsId));
            }
            $this->instances[$newsId] = $news;
        }

        return $this->instances[$newsId];
    }

    /**
     * @param NewsInterface $news
     * @return bool true on success
     * @throws CouldNotDeleteException
     */
    public function delete(newsInterface $news)
    {
        try {
            $faqId = $news->getId();
            $this->resource->delete($news);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        unset($this->instances[$faqId]);
        return true;
    }

    /**
     * @param int $newsId
     * @return bool true on success
     */
    public function deleteById($newsId)
    {
        return $this->delete($this->getById($newsId));
    }


    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \RSHB\News\Api\Data\NewsSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var NewsCollectionFactory $collection */
        $collection = $this->newsFactory->create();
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        /** @var NewsSearchResultInterface $searchResult */
        $searchResult = $this->newsSearchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());
        return $searchResult;
    }
}

<?php

namespace RSHB\News\Model\Attribute\Frontend;

class Material extends \Magento\Eav\Model\Entity\Attribute\Frontend\AbstractFrontend
{
    public function getValue(\Magento\Framework\DataObject $object)
    {
        $value = $object->getData($this->getAttribute()->getAttributeCode());
        if ($value == 1) {
            return "<b>Yes</b>";
        } else {
            return "<b>No</b>";
        }
    }
}

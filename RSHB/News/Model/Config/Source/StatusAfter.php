<?php

namespace RSHB\News\Model\Config\Source;

class StatusAfter implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 0, 'label' => __('Enable')],
            ['value' => 1, 'label' => __('Disabled')],
        ];
    }
}

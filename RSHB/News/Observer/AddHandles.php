<?php

namespace RSHB\News\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Session as CustomerSession;
use RSHB\News\Model\NewsRepository;

class AddHandles implements ObserverInterface
{
    protected $_pageFactory;

    protected $_postFactory;

    protected $_newsRepository;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \RSHB\News\Model\NewsFactory $newsFactory,
        NewsRepository $newsRepository
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_newsFactory = $newsFactory;
        $this->_newsRepository = $newsRepository;
    }


    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $news = $this->_newsFactory->create();
        $news->setTitle('Test title');
        $news->setIntrotext('Test introtext');
        $this->_newsRepository->save($news);
    }
}



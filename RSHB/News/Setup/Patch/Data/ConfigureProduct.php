<?php

namespace RSHB\News\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Framework\App\State;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;

class ConfigureProduct implements DataPatchInterface, PatchRevertableInterface
{
    private $moduleDataSetup;

    protected $newProduct;

    private $state;
    private $options_factory;

    private $_objectManager;

    private $ProductInterfaceFactory;

    private $productRepository;

    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Magento\ConfigurableProduct\Helper\Product\Options\Factory $options_factory,
        \Magento\Catalog\Api\ProductRepositoryInterface $_productRepository,
        Product $newProduct,
        \Magento\Catalog\Api\Data\ProductInterfaceFactory $ProductInterfaceFactory,
        State $state
    ) {
        $this->_objectManager = $objectmanager;
        $this->newProduct = $newProduct;
        $this->state = $state;
        $this->ProductInterfaceFactory = $ProductInterfaceFactory;

        $this->productRepository = $_productRepository;
        $this->options_factory = $options_factory;
    }

    public function apply()
    {
        $state = $this->state;
        $state->setAreaCode(
            \Magento\Framework\App\Area::AREA_ADMINHTML
        ); // or \Magento\Framework\App\Area::AREA_FRONTEND, depending on your need
        try {
            $productInterfaceFactory = $this->ProductInterfaceFactory;
            $productRepository = $this->productRepository;
            $optionsFactory = $this->options_factory;

            $product = $productInterfaceFactory->create();

            // Get the default attribute set id
            $attributeSetId = $product->getDefaultAttributeSetId();

            $attributeLabel = 'shirts';
            $attributeCode = 'shirts';
            $attribute = $product->getResource()->getAttribute($attributeCode);
            $options = $attribute->getOptions();
            array_shift($options); // Remove the first option which is empty
            $simpleProductIds = $attributeValues = [];
            foreach ($options as $key => $option) {
                $simpleProduct = $productInterfaceFactory->create();
                $sku = $urlKey = 'skirt***_-' . strtolower($option->getLabel());
                $simpleProduct->setSku($sku)
                    ->setName('skirt***-' . $option->getLabel())
                    ->setUrlKey($urlKey)
                    ->setAttributeSetId($attributeSetId)
                    ->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED)
                    ->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH)
                    ->setTypeId(\Magento\Catalog\Model\Product\Type::TYPE_SIMPLE)
                    ->setWeight(1) // Weight of the product
                    ->setTaxClassId(0) // Tax class id, 0 = None, 2 = Taxable Goods, etc.
                    ->setPrice(100) // Price of the product
                    ->setTextSwatchAttribute($option->getValue())
                    ->setWebsiteIds([1]) // Assign product to websites
                    ->setShirts($option->getValue()) // Assign product to websites
                    ->setStockData(
                        [
                            'use_config_manage_stock' => 0,
                            'manage_stock' => 1,
                            'is_in_stock' => 1,
                            'qty' => 999999
                        ]
                    );
                echo 'rtgtrg';
                //  $simpleProduct->setData('shirts',$option->getLabel());
                $simpleProducts = $productRepository->save($simpleProduct);
                if ($simpleProducts->getId()) {
                    $simpleProductIds[] = $simpleProducts->getId();
                    $attributeValues[] = [
                        'label' => $attributeLabel,
                        'attribute_id' => $attribute->getId(),
                        'value_index' => $option->getValue()
                    ];
                }
            }
            $product = $productInterfaceFactory->create();
            $configurableAttributesData = [
                [
                    'attribute_id' => $attribute->getId(),
                    'code' => $attribute->getAttributeCode(),
                    'label' => $attribute->getStoreLabel(),
                    'position' => '0',
                    'values' => $attributeValues
                ]
            ];
            $configurableOptions = $optionsFactory->create($configurableAttributesData);

            $extensionConfigurableAttributes = $product->getExtensionAttributes();
            $extensionConfigurableAttributes->setConfigurableProductOptions($configurableOptions);
            $extensionConfigurableAttributes->setConfigurableProductLinks($simpleProductIds);

            $product->setExtensionAttributes($extensionConfigurableAttributes);
            $sku = $urlKey = '--_Red2 skirs***--';
            $product->setSku($sku)
                ->setName('--_Red2 skirs***--')
                ->setUrlKey($urlKey)
                ->setAttributeSetId($attributeSetId)
                ->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED)
                ->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH)
                ->setTypeId(\Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE)
                ->setWebsiteIds([1])
                ->setCategoryIds([5]) // Assign product to categories using category id
                ->setStockData(
                    [
                        'use_config_manage_stock' => 1,
                        'is_in_stock' => 1
                    ]
                );

            $products = $productRepository->save($product);

            if ($products->getId()) {
                echo "Product Created Successfully";
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        exit();
    }

    public static function getDependencies()
    {
        return [

        ];
    }

    public function revert()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        //Here should go code that will revert all operations from `apply` method
        //Please note, that some operations, like removing data from column, that is in role of foreign key reference
        //is dangerous, because it can trigger ON DELETE statement
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        /**
         * This internal Magento method, that means that some patches with time can change their names,
         * but changing name should not affect installation process, that's why if we will change name of the patch
         * we will add alias here
         */
        return [];
    }
}

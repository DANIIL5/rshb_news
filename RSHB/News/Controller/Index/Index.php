<?php

namespace RSHB\News\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use RSHB\News\Model\NewsRepository;
use RSHB\News\Api\Data\AttributeInterfaceFactory;
use Magento\Sales\Api\OrderRepositoryInterface;

class Index extends Action
{
    protected $_pageFactory;

    protected $_newsRepository;

    protected $dataHelper;

    protected $_blenders;
    /**
     * @var OrderRepositoryInterface
     */
    private $OrderrepositoryInterface;

    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \RSHB\News\Model\NewsFactory $newsFactory,
        NewsRepository $newsRepository,
        //\RSHB\News\Model\BlenderFactory $_blender,
        //\RSHB\News\Model\Blender $_blenders,
        \RSHB\News\Helper\Data $dataHelper,
        AttributeInterfaceFactory $AttributeInterfaceFactory,
        OrderRepositoryInterface $OrderrepositoryInterface
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_newsFactory = $newsFactory;
        $this->_newsRepository = $newsRepository;
        // $this->_blenderFactory = $_blender;
        //$this->_blenders = $_blenders;
        $this->dataHelper = $dataHelper;
        $this->AttributeInterfaceFactory = $AttributeInterfaceFactory;
        $this->OrderrepositoryInterface = $OrderrepositoryInterface;
        return parent::__construct($context);
    }

    public function execute()
    {
        //$this->_blenders;
        //  $this->_blenderFactory->create();
        // $post = $this->_newsFactory->create();
        // $collection = $post->getCollection();
        // foreach ($collection as $item) {
        //    echo "<pre>";
        //   print_r($item->getData());
        //   echo "</pre>";
        // }
        //echo $this->dataHelper->getGeneralConfig('enable');
        //echo $this->dataHelper->getGeneralConfig('display_text');
        //$get_news= $this->_newsRepository->getById(61);
        //print_r($get_news->getId());
        // $news = $this->_newsFactory->create();
        //  $news->setTitle('Test title');
        // $news->setIntrotext('Test introtext');
        // $this->_newsRepository->save($news);
        $order = $this->OrderrepositoryInterface->get(2);
        $extensionAttribute = $order->getExtensionAttributes();
        $orderCustomAttribute = $extensionAttribute->getOrderCustomAttribute() ?: $this->AttributeInterfaceFactory->create();
        $extensionAttribute->setOrderCustomAttribute($orderCustomAttribute);
        if ($orderCustomAttribute) {
            $orderCustomAttribute->setBar('898989');
            $orderCustomAttribute->setOrderId($order->getEntityId());
        }
        $this->OrderrepositoryInterface->save($order);
    }
}

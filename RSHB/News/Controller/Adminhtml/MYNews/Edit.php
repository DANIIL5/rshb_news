<?php

namespace RSHB\News\Controller\Adminhtml\MYNews;

use RSHB\News\Model\NewsRepository;
use Magento\Backend\App\Action;

class Edit extends \Magento\Backend\App\Action
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_newsRepository;
    protected $_coreRegistry;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        NewsRepository $newsRepository,
        \RSHB\News\Model\NewsFactory $newsFactory
    ) {
        $this->_newsRepository = $newsRepository;
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        $this->_newsFactory = $newsFactory;
        parent::__construct($context);
    }

    /**
     * Authorization level
     *
     * @see _isAllowed()
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('RSHB_News::save');
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Mynews
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Mynews $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('RSHB_News::news_mynews')
            ->addBreadcrumb(__('News'), __('News'))
            ->addBreadcrumb(__('Manage All News'), __('Manage All News'));
        return $resultPage;
    }

    /**
     * Edit Allnews
     *
     * @return \Magento\Backend\Model\View\Result\Mynews|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('Id');
        if (isset($id)) {
            $model = $this->_newsRepository->getById($id);
        } else {
            $model = $this->_newsFactory->create();
        }

        // 2. Initial checking
        if ($id) {
            if (!$model->getId()) {
                $this->messageManager->addError(__('This news no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->_coreRegistry->register('news_mynews', $model);

        // 5. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Mynews $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit News') : __('Add News'),
            $id ? __('Edit News') : __('Add News')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Allnews'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? $model->getTitle() : __('Add News'));
        return $resultPage;
    }
}

<?php
// app/code/[Vendor]/[Module]/Api/FaqRepositoryInterface.php

namespace RSHB\News\Api;

use Magento\Framework\Exception\NoSuchEntityException;
use RSHB\News\Api\Data\NewsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface NewsRestApiInterface
{
    /**
     * Get customer's name by Customer ID and return greeting message.
     *
     * @param int $customerId
     * @return \Magento\Customer\Api\Data\CustomerInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If customer with the specified ID does not exist.
     * @throws \Magento\Framework\Exception\LocalizedException
     * @api
     */
    public function sayHello(int $customerId);

    /**
     * @param int $newsId
     * @return NewsInterface
     * @throws NoSuchEntityException
     * *@api
     */
    public function getById(int $newsId);

    /**
     * @param int $newsId
     * @return bool true on success
     */
    public function deleteById($newsId);

    /**
     * Save news
     *
     * @param \RSHB\News\Api\Data\NewsInterface $news
     * @return \RSHB\News\Api\Data\NewsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\NewsInterface $news);


    /**
     * Save news
     *
     * @param \RSHB\News\Api\Data\NewsInterface $news
     * @return \RSHB\News\Api\Data\NewsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function editStatus(Data\NewsInterface $news);


    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \RSHB\News\Api\Data\NewsSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}

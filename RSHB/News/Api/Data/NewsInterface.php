<?php


namespace RSHB\News\Api\Data;

interface NewsInterface
{

    /**
     * Constants for keys of data array.
     */
    const NEWS_ID = 'Id';
    const TITLE = 'Title';
    const INTROTEXT = 'Introtext';
    const IMAGE = 'Image';
    const CREATED_AT = 'Created_at';
    const CHANGED_AT = 'Changed_at';
    const PUBLISHED_FROM = 'Published_from';
    const PUBLISHED_TO = 'Published_to';
    const STATUS = 'Status';
    const CONTENT = 'Content';

    /**
     * Get id
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set id
     *
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle();

    /**
     * Set title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * Get content
     *
     * @return string|null
     */
    public function getIntrotext();


    /**
     * Get Status
     *
     * @return int|null
     */
    public function getStatus();

    /**
     * Set status
     *
     * @param int $status
     * @return int
     */
    public function setStatus($status);

    /**
     * Set content
     *
     * @param string $content
     * @return $this
     */
    public function setIntrotext($content);


}

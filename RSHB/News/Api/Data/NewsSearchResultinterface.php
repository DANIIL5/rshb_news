<?php

namespace RSHB\News\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface PostSearchResultInterface
 * @package AlexPoletaev\Blog\Api\Data
 */
interface NewsSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return \RSHB\News\Api\Data\NewsInterface[]
     */
    public function getItems();

    /**
     * @param \RSHB\News\Api\Data\NewsInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

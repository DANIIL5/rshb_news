<?php

// app/code/[Vendor]/[Module]/Api/FaqRepositoryInterface.php

namespace RSHB\News\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface NewsRepositoryInterface
{

    /**
     * Save news
     *
     * @param \RSHB\News\Api\Data\NewsInterface $news
     * @return \RSHB\News\Api\Data\NewsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\NewsInterface $news);

    /**
     * Retrieve news
     *
     * @param int $newsId
     * @return \RSHB\News\Api\Data\NewsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($newsId);


    /**
     * Delete news
     *
     * @param \RSHB\News\Api\Data\NewsInterface $news
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(Data\NewsInterface $news);

    /**
     * Delete news by ID
     *
     * @param int $newsId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($newsId);


    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \RSHB\News\Api\Data\NewsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

}
